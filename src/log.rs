extern crate slog;
extern crate slog_async;
extern crate slog_json;
extern crate slog_stdlog;
extern crate slog_term;

use slog::Drain;
use std::{fmt, io, result, sync::Mutex};

static mut CURRENT_LOGLEVEL: slog::FilterLevel = slog::FilterLevel::Info;

pub static DEFAULT_LOGLEVEL: slog::FilterLevel = slog::FilterLevel::Info;

// gets current minimum log level
pub fn get_log_level() -> slog::FilterLevel {
    // unsafe because we don't care about race conditions in setting a new log level
    unsafe { CURRENT_LOGLEVEL.clone() }
}

pub fn set_log_level(l: slog::FilterLevel) {
    // unsafe because we don't care about race conditions in setting a new log level
    unsafe {
        CURRENT_LOGLEVEL = l;
    }
}

struct RuntimeLevelFilter<D> {
    drain: D,
}
struct PanicFilter<D> {
    drain: D,
}

impl<D: Drain> Drain for RuntimeLevelFilter<D> {
    type Ok = Option<D::Ok>;
    type Err = Option<D::Err>;

    fn log(
        &self,
        record: &slog::Record,
        values: &slog::OwnedKVList,
    ) -> result::Result<Self::Ok, Self::Err> {
        unsafe {
            if CURRENT_LOGLEVEL.accepts(record.level()) {
                self.drain.log(record, values).map(Some).map_err(Some)
            } else {
                Ok(None)
            }
        }
    }
}

impl<D: Drain> Drain for PanicFilter<D> {
    type Ok = Option<D::Ok>;
    type Err = Option<D::Err>;

    fn log(
        &self,
        record: &slog::Record,
        values: &slog::OwnedKVList,
    ) -> result::Result<Self::Ok, Self::Err> {
        match self.drain.log(record, values).map(Some).map_err(Some) {
            Ok(_) => (),
            Err(_) => (),
        }
        if slog::Level::Critical == record.level() {
            std::process::exit(1);
        }
        Ok(None)
    }
}

fn generate_root_logger<T: 'static>(drain: T) -> slog::Logger
where
    T: slog::Drain + Send,
    T::Err: fmt::Debug,
{
    let drain = RuntimeLevelFilter { drain: drain }.fuse();
    let drain = PanicFilter { drain: drain }.fuse();
    let drain = slog_async::Async::new(drain).build().fuse();

    slog::Logger::root(drain, o!("version" => env!("CARGO_PKG_VERSION")))
}

pub fn make_logger(format: &str) -> slog::Logger {
    match format {
        "json" => generate_root_logger(Mutex::new(slog_json::Json::default(io::stderr()))),
        "pretty" => generate_root_logger(
            slog_term::FullFormat::new(slog_term::TermDecorator::new().build()).build(),
        ),
        _ => {
            let log = make_logger("json");
            error!(log, "unknown logging format"; "format" => format);
            log
        }
    }
}
