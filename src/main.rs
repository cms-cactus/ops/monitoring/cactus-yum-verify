#[macro_use]
extern crate slog;
extern crate slog_scope;
extern crate slog_stdlog;
#[macro_use]
extern crate lazy_static;

use std::io::{self, Write};

mod api;
mod cli;
mod config;
mod log;
mod yum_verify;

lazy_static! {
    static ref CLI: clap::ArgMatches<'static> = cli::parse();
    static ref LOG: slog::Logger = log::make_logger(CLI.value_of("log.format").unwrap());
    static ref CONFIG: config::Config = config::parse(&CLI);
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let _scope_guard = slog_scope::set_global_logger(LOG.new(o!()));
    let _log_guard = slog_stdlog::init().unwrap();

    if log::get_log_level() != CONFIG.log_level {
        info!(LOG, "setting log level"; "level" => &CONFIG.log_level.as_str());
        log::set_log_level(CONFIG.log_level);
    }

    if CONFIG.packages.len() == 0 {
        cli::build_cli().print_help().unwrap();
        io::stderr().write_all(b"\n")?;
        crit!(LOG, "no packages in config to check");
    }

    info!(LOG, "packages that will be verified"; "packages" => &CONFIG.packages.join(", "));

    api::start().await;

    Ok(())
}
