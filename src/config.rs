extern crate clap;
extern crate config;
extern crate slog;

use core::str::FromStr;
use std::env;

use super::log::DEFAULT_LOGLEVEL;
use super::LOG;

const DEFAULT_CONFIG_FILE: &str = "config";

pub struct Config {
    pub log_level: slog::FilterLevel,
    pub packages: Vec<String>,
    pub http_port: u16,
}

pub fn parse(cli: &clap::ArgMatches) -> Config {
    // cli override > env variable > default
    let cli_config_file = (*cli.value_of("config").unwrap()).to_string();
    let config_file = if cli.occurrences_of("config") == 0 {
        env::var("YUM_VERIFY_CONFIG").unwrap_or(cli_config_file)
    } else {
        cli_config_file
    };
    info!(LOG, "loading config file"; "path" => &config_file);

    let mut cfile = config::Config::default();
    match cfile.merge(config::File::with_name(&config_file)) {
        Err(e) => match &config_file[..] {
            DEFAULT_CONFIG_FILE => {
                warn!(LOG, "could not read default config file");
            }
            _ => {
                crit!(LOG, "unable to read config file"; "file" => &config_file, "error" => e.to_string());
            }
        },
        Ok(_config) => (),
    }

    let log_level = get(&cli, "log.level", &cfile, "log.level", "LOG_LEVEL");
    let log_level = slog::FilterLevel::from_str(log_level.as_ref()).unwrap_or_else(|_e| {
        error!(LOG, "unknown log level requested"; "level" => log_level);
        DEFAULT_LOGLEVEL
    });

    let result = Config {
        log_level: log_level,
        packages: get_all(&cli, "package", &cfile, "packages", "PACKAGES"),
        http_port: get(&cli, "http.port", &cfile, "http.port", "HTTP_PORT")
            .parse::<u16>()
            .expect("invalid port given"),
    };
    return result;
}

fn get<'a, 'b>(
    cli: &'a clap::ArgMatches,
    cli_key: &'b str,
    c: &'a config::Config,
    config_key: &'b str,
    env_key: &'b str,
) -> String {
    let cli_val = cli.value_of(cli_key);
    let config_val = c.get(config_key);
    let env_val = env::var(&["YUM_VERIFY", env_key].concat());

    if cli.occurrences_of(cli_key) != 0 {
        return cli_val.unwrap().to_string();
    }

    // no cli override, config key > env key > cli default
    return config_val.unwrap_or(env_val.unwrap_or(cli_val.unwrap().to_string()));
}

fn get_all<'a, 'b>(
    cli: &'a clap::ArgMatches,
    cli_key: &'b str,
    c: &'a config::Config,
    config_key: &'b str,
    env_key: &'b str,
) -> Vec<String> {
    cli.values_of(cli_key)
        .unwrap_or(clap::Values::default())
        .map(|s| s.to_owned())
        .chain(
            c.get_array(config_key)
                .unwrap_or_default()
                .iter()
                .map(|k| k.clone().into_str().unwrap()),
        )
        .chain(
            env::var(&["YUM_VERIFY", env_key].concat())
                .unwrap_or_default()
                .split(",")
                .filter(|&x| !x.is_empty())
                .map(|s| s.to_owned()),
        )
        .collect::<Vec<String>>()
}
