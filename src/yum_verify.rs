use super::CONFIG;
use super::LOG;
use lazy_static::lazy_static;
use prometheus::{opts, register_counter, register_int_counter_vec, Counter, IntCounterVec};
use std::io::{BufRead, BufReader, Error, ErrorKind};
use std::process::{Command, Stdio};

#[cfg(test)]
use mockall::{automock, predicate::*};

const PROBLEM_FILE_PREFIX: &str = "    File: ";
const PROBLEM_PREFIX: &str = "        Problem:  ";

pub struct Problem {
    pub package: String,
    pub file: String,
    pub problem: String,
}
impl Problem {
    pub fn new(package: String, file: String, problem: String) -> Problem {
        Problem {
            package: package,
            file: file,
            problem: problem,
        }
    }

    #[cfg(test)]
    fn matches(&self, package: &str, file: &str, problem: &str) -> bool {
        self.package == package && self.file == file && self.problem == problem
    }
}
impl slog::Value for Problem {
    fn serialize(
        &self,
        _rec: &slog::Record,
        key: slog::Key,
        serializer: &mut dyn slog::Serializer,
    ) -> Result<(), slog::Error> {
        serializer.emit_str(
            key,
            format!("{}::{}::{}", self.package, self.file, self.problem).as_ref(),
        )
    }
}

#[cfg_attr(test, automock)]
pub trait YumVerifier {
    fn run_yum_verify(&self) -> std::result::Result<std::process::Child, std::io::Error>;
}

struct RealVerifier {}
impl YumVerifier for RealVerifier {
    fn run_yum_verify(&self) -> std::result::Result<std::process::Child, std::io::Error> {
        let cmd = format!("yum verify {}", CONFIG.packages.join(" "));
        trace!(LOG, "command"; "command" => &cmd);
        Command::new("sh")
            .arg("-c")
            .arg(cmd)
            .stderr(Stdio::piped())
            .stdout(Stdio::piped())
            .spawn()
    }
}

lazy_static! {
    static ref VERIFY_COUNTER: Counter = register_counter!(opts!(
        "yum_verify_total",
        "Number of successful yum-verify calls."
    ))
    .unwrap();
    static ref VERIFY_COUNTER_FAIL: Counter = register_counter!(opts!(
        "yum_verify_fail_total",
        "Number of failed yum-verify calls."
    ))
    .unwrap();
    static ref VERIFY_PROBLEM: IntCounterVec = register_int_counter_vec!(
        "yum_verify_problem_total",
        "Encountered problems while running yum verify",
        &["package", "file", "problem"]
    )
    .unwrap();
}

pub fn verify() {
    match get_problems(RealVerifier {}) {
        Err(e) => {
            error!(LOG, "yum verify command failed to execute"; "error" => e);
            VERIFY_COUNTER_FAIL.inc();
        }
        Ok(problems) => {
            debug!(LOG, "received yum-verify problems"; "problems count" => problems.len());
            VERIFY_PROBLEM.reset();
            problems.iter().for_each(|problem| {
                trace!(LOG, "problem"; "problem" => problem);
                VERIFY_PROBLEM
                    .with_label_values(&[&problem.package, &problem.file, &problem.problem])
                    .inc();
            })
        }
    }
}

fn log_output(kind: &str, bytes: &Vec<u8>) -> Result<(), Error> {
    let s = String::from_utf8(bytes.to_vec()).or_else(|e| {
        error!(LOG, "failure to read output"; "kind" => kind, "error" => e.to_string());
        Err(Error::new(ErrorKind::Other, "oh no!"))
    })?;
    trace!(LOG, "process output"; "kind" => kind, "str" => s);
    Ok(())
}

pub fn get_problems<T>(verifier: T) -> Result<Vec<Problem>, Error>
where
    T: YumVerifier,
{
    let process = match verifier.run_yum_verify() {
        Ok(p) => p,
        Err(e) => {
            error!(LOG, "cannot execute yum-verify command"; "error" => &e);
            return Err(e);
        }
    };
    let process = process.wait_with_output()?;

    let mut current_package = "".to_owned();
    let mut current_file = "".to_owned();

    let mut problems: Vec<Problem> = Vec::new();

    log_output("stderr", &process.stderr)?;
    log_output("stdout", &process.stdout)?;

    let lines = BufReader::new(process.stdout.as_slice())
        .lines()
        .enumerate()
        .skip(2);
    for (_, line) in lines {
        let line = match line {
            Ok(l) => l,
            Err(e) => {
                error!(LOG, "cannot read yum-verify output"; "error" => &e);
                return Err(e);
            }
        };

        if line.len() == 0 {
            continue;
        }

        // new package names occur on a new line with no leading spaces
        if !line.starts_with(" ") {
            match line.find(" : ") {
                Some(index) => {
                    current_package = line[..index].trim_end_matches(".x86_64").to_owned();
                }
                None => (),
            };
        }
        // new problematic file name within a package
        else if line.starts_with(PROBLEM_FILE_PREFIX) {
            current_file = line[PROBLEM_FILE_PREFIX.len()..].to_owned();
        }
        // new problem statement for a file within a package
        else if line.starts_with(PROBLEM_PREFIX) {
            let problem = &line[PROBLEM_PREFIX.len()..].to_string();
            problems.push(Problem::new(
                current_package.clone(),
                current_file.clone(),
                problem.clone(),
            ));
        }
    }

    Ok(problems)
}

#[cfg(test)]
mod tests {
    use super::*;

    const SHITTY_PACKAGES_RESPONSE: &str = include_str!("../docs/yum-verify/fail.stdout");
    const HAPPY_PACKAGES_RESPONSE: &str = include_str!("../docs/yum-verify/success.stdout");

    fn get_mock(expected_response: &'static &str) -> MockYumVerifier {
        let mut mock = MockYumVerifier::new();
        mock.expect_run_yum_verify().times(1).returning(move || {
            Command::new("sh")
                .arg("-c")
                .arg(format!("echo '{}'", expected_response))
                .stderr(Stdio::piped())
                .stdout(Stdio::piped())
                .spawn()
        });
        mock
    }

    #[test]
    fn failure_does_not_fail_at_failing() -> Result<(), Error> {
        let problems = get_problems(get_mock(&SHITTY_PACKAGES_RESPONSE))?;

        assert_eq!(12, problems.len());
        assert!(problems[0].matches(
            "cactus-grafana",
            "/opt/cactus/grafana/conf/grafana.ini",
            "checksum does not match"
        ));
        assert!(problems[1].matches(
            "cactus-grafana",
            "/opt/cactus/grafana/conf/grafana.ini",
            "size does not match"
        ));
        assert!(problems[2].matches(
            "cactus-grafana",
            "/opt/cactus/grafana/conf/grafana.ini",
            "mtime does not match"
        ));

        assert!(problems[3].matches(
            "cactus-prometheus",
            "/opt/cactus/prometheus/conf.P5.yaml",
            "checksum does not match"
        ));
        assert!(problems[4].matches(
            "cactus-prometheus",
            "/opt/cactus/prometheus/conf.P5.yaml",
            "size does not match"
        ));
        assert!(problems[5].matches(
            "cactus-prometheus",
            "/opt/cactus/prometheus/conf.P5.yaml",
            "mtime does not match"
        ));

        assert!(problems[6].matches(
            "cactus-prometheus",
            "/opt/cactus/prometheus/rules/blackbox_exporter.yml",
            "checksum does not match"
        ));
        assert!(problems[7].matches(
            "cactus-prometheus",
            "/opt/cactus/prometheus/rules/blackbox_exporter.yml",
            "size does not match"
        ));
        assert!(problems[8].matches(
            "cactus-prometheus",
            "/opt/cactus/prometheus/rules/blackbox_exporter.yml",
            "mtime does not match"
        ));

        assert!(problems[9].matches(
            "cactus-prometheus",
            "/opt/cactus/prometheus/rules/node_exporter-systemd.yml",
            "checksum does not match"
        ));
        assert!(problems[10].matches(
            "cactus-prometheus",
            "/opt/cactus/prometheus/rules/node_exporter-systemd.yml",
            "size does not match"
        ));
        assert!(problems[11].matches(
            "cactus-prometheus",
            "/opt/cactus/prometheus/rules/node_exporter-systemd.yml",
            "mtime does not match"
        ));
        Ok(())
    }

    #[test]
    fn zero_failures_produce_empty_problem_vec() -> Result<(), Error> {
        let problems = get_problems(get_mock(&HAPPY_PACKAGES_RESPONSE))?;
        assert_eq!(0, problems.len());
        Ok(())
    }
}
