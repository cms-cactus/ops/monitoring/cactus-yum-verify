// https://github.com/seanmonstar/warp/blob/master/examples/todos.rs

use super::{CONFIG, LOG};

pub async fn start() {
    warp::serve(filters::all())
        .run(([0, 0, 0, 0], CONFIG.http_port))
        .await;
}

mod filters {
    use super::handlers;
    use super::LOG;
    use warp::Filter;

    pub fn all() -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        let log = warp::log::custom(|info| {
            // https://docs.rs/warp/0.1.10/src/warp/filters/log.rs.html#33-53
            debug!(LOG, "API request handled"; "method" => info.method().as_str(), "path" => info.path(), "status" => info.status().as_u16(), "elapsed" => format!("{:?}", info.elapsed()))
        });
        get_root().or(get_metrics()).with(log)
    }

    pub fn get_root() -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
        warp::path::end()
            .and(warp::get())
            .and_then(handlers::get_root)
    }

    pub fn get_metrics() -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone
    {
        warp::path!("metrics")
            .and(warp::get())
            .and_then(move || handlers::get_metrics())
    }
}

pub mod handlers {
    use super::super::yum_verify;
    use super::LOG;
    use prometheus::{Encoder, TextEncoder};
    use std::convert::Infallible;
    use warp::http::StatusCode;

    pub async fn get_root() -> Result<impl warp::Reply, Infallible> {
        Ok(StatusCode::OK)
    }

    pub async fn get_metrics() -> Result<impl warp::Reply, Infallible> {
        debug!(LOG, "getting metrics");
        yum_verify::verify();
        let encoder = TextEncoder::new();
        let metric_families = prometheus::gather();
        let mut buffer = vec![];
        encoder.encode(&metric_families, &mut buffer).unwrap();
        Ok(String::from_utf8(buffer).unwrap())
    }
}
