extern crate clap;
extern crate config;
extern crate slog;

use clap::{App, AppSettings, Arg};
use std::env;

pub fn build_cli() -> App<'static, 'static> {
    App::new("Cactus Yum Verify Exporter")
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .setting(AppSettings::ColoredHelp)
        .arg(
            Arg::with_name("log.level")
                .short("l")
                .long("log.level")
                .takes_value(true)
                .default_value("info")
                .help("set lowest reported log level (or 'off')"),
        )
        .arg(
            Arg::with_name("log.format")
                .long("log.format")
                .takes_value(true)
                .default_value("pretty")
                .help("set logging format (json|pretty)"),
        )
        .arg(
            Arg::with_name("config")
                .short("c")
                .long("config.file")
                .takes_value(true)
                .default_value("config")
                .help("Specify config file name"),
        )
        .arg(
            Arg::with_name("package")
                .short("p")
                .long("package")
                .multiple(true)
                .takes_value(true)
                .help("Specify a package selector"),
        )
        .arg(
            Arg::with_name("http.port")
                .long("http.port")
                .takes_value(true)
                .default_value("8085")
                .help("Specify a port to listen on"),
        )
}

pub fn parse<'a>() -> clap::ArgMatches<'a> {
    build_cli().get_matches()
}
