FROM cern/cc7-base:20210401-1.x86_64

ARG RUST_VERSION=stable

LABEL maintainer="Cactus <cactus@cern.ch>"

# source scl_source enable rh-git218 rh-ruby27 + source /root/.cargo/env
ENV PATH="/opt/rh/rh-ruby27/root/usr/local/bin:/opt/rh/rh-ruby27/root/usr/bin:/opt/rh/rh-git218/root/usr/bin:/root/.cargo/bin:$PATH"
ENV LD_LIBRARY_PATH="/opt/rh/rh-ruby27/root/usr/local/lib64:/opt/rh/rh-ruby27/root/usr/lib64:/opt/rh/httpd24/root/usr/lib64:/usr/lib64:$LD_LIBRARY_PATH"
ENV MANPATH="/opt/rh/rh-ruby27/root/usr/local/share/man:/opt/rh/rh-ruby27/root/usr/share/man:/opt/rh/rh-git218/root/usr/share/man:$MANPATH"
ENV PERL5LIB="/opt/rh/rh-git218/root/usr/share/perl5/vendor_perl"
ENV X_SCLS="rh-ruby27 rh-git218"
ENV XDG_DATA_DIRS="/opt/rh/rh-ruby27/root/usr/local/share:/opt/rh/rh-ruby27/root/usr/share:/usr/local/share:/usr/share:$XDG_DATA_DIRS"
ENV PKG_CONFIG_PATH="/opt/rh/rh-ruby27/root/usr/local/lib64/pkgconfig:/opt/rh/rh-ruby27/root/usr/lib64/pkgconfig:$PKG_CONFIG_PATH"

RUN yum install -y make rh-git218 gcc rpm-build rh-ruby27 rh-ruby27-ruby-devel && \
    gem install fpm && \
    yum clean all

# musl for static binary builds (no old glibc at P5 issues)
RUN curl -LSs https://www.musl-libc.org/releases/musl-1.2.2.tar.gz -o musl.tar.gz && \
    tar -xvf musl.tar.gz && \
    cd musl-1.2.2 && \
    ./configure && \
    make && \
    make install && \
    cd .. && \
    rm -rf musl*

RUN curl https://sh.rustup.rs -sSf | bash -s -- -y && \
    source $HOME/.cargo/env && \
    rustup default ${RUST_VERSION}-x86_64-unknown-linux-musl
