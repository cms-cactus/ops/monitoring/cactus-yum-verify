
##
### Docker commands
##

BUILDER_IMAGE=gitlab-registry.cern.ch/cms-cactus/ops/monitoring/cactus-yum-verify/builder

docker.images: ## build docker images used for local running
	docker pull ${BUILDER_IMAGE} || echo "docker pull failed, no cache for you"
	cat builder.Dockerfile | docker build -t ${BUILDER_IMAGE} -


IMAGE ?= ansible
docker.bash: ## open a shell in a runner container
	docker run -it --rm --mount type=bind,source=$(shell pwd),target=/mnt --workdir /mnt ${BUILDER_IMAGE} bash

docker.%: ## run any make target in a docker container
	docker run -it --rm --mount type=bind,source=$(shell pwd),target=/mnt --workdir /mnt ${BUILDER_IMAGE} "make $(subst docker.,,$@)"
