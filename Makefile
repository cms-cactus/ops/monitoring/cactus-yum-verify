SHELL:=/bin/bash
.DEFAULT_GOAL := help


# if CI tag name is set, we use that as version
ifdef CI_COMMIT_TAG
	VERSION := ${CI_COMMIT_TAG}
# if not, we set it to 0.0.0-<git short hash>
else
	CI_COMMIT_SHORT_SHA ?= $(shell git rev-parse --short HEAD)
	VERSION := 0.0.0-${CI_COMMIT_SHORT_SHA}
endif

# if there's uncommitted changes, we add -dirty to the version string
ifneq ($(shell git diff --shortstat -- . ':(exclude)Cargo.toml' ':(exclude)Cargo.lock' 2> /dev/null | tail -n1),)
	DIFF := $(shell git diff --name-only -- . ':(exclude)Cargo.toml' ':(exclude)Cargo.lock')
	VERSION := ${VERSION}-dirty
$(warning source is dirty)
$(warning ${DIFF})
endif

RELEASE ?= 1
ARCH=x86_64
RPM_NAME = cactus-yum-verify-${VERSION}-${RELEASE}.${ARCH}.rpm

src_files := $(shell find -name '*.rs')

##
### Main targets
##

.PHONY: build
build: target/x86_64-unknown-linux-musl/release/cactus-yum-verify ## build the software for release
target/x86_64-unknown-linux-musl/release/cactus-yum-verify: $(src_files) Cargo.toml
	-@$(MAKE) git-filters > /dev/null 2> /dev/null
	sed -i "s|^version = .*$$|version = \"${VERSION}\"|" Cargo.toml
	cargo build --release

.PHONY: test
test: ## run tests
	cargo test

.PHONY: lint
lint: ## format code
	cargo fmt

.PHONY: clean
clean: ## clean up
	cargo clean

.PHONY: rpm
rpm: ${RPM_NAME} ## generate RPM
${RPM_NAME}: build
	rm -rf rpmroot
	mkdir -p rpmroot/opt/cactus/yum-verify rpmroot/usr/lib/systemd/system
	cp P5/cactus-yum-verify.service rpmroot/usr/lib/systemd/system
	cp target/x86_64-unknown-linux-musl/release/cactus-yum-verify P5/config.yaml rpmroot/opt/cactus/yum-verify
	cd rpmroot && fpm \
	-s dir \
	-t rpm \
	-n cactus-yum-verify \
	-v ${VERSION} \
	-a ${ARCH} \
	-m "<cactus@cern.ch>" \
	--iteration ${RELEASE} \
	--vendor CERN \
	--description "yum-verify for the L1 Online Software at P5" \
	--url "https://gitlab.cern.ch/cms-cactus/ops/monitoring/cactus-yum-verify" \
	--provides cactus_yum_verify \
	.=/ && mv *.rpm ..

##
### Utility targets
##

# https://git-scm.com/book/en/v2/Customizing-Git-Git-Attributes
.PHONY: git-filters
git-filters: ## set up git filters in config
	git config filter.autoversion.clean 'perl -0777 -pe "s|name = \"cactus-yum-verify\"\nversion = [^\n]*|name = \"cactus-yum-verify\"\nversion = \"0.0.0\"|igs"'

.PHONY: build-dev
build-dev: ## build debug binary
	cargo build

##
### CI helpers
##

.PHONY: check-linted
check-linted: ## verify that code is properly formatted
	cargo fmt
	@git diff --name-only --exit-code -- . ':(exclude)Cargo.toml' || (echo "ERROR: you committed malformatted code, run 'make lint'" && exit 1)
	@grep 'version = "0.0.0"' Cargo.toml > /dev/null || (echo "ERROR: version string in Cargo.toml is automated. Do not commit a change to version" && exit 1)

include .makefile/dockerized.mk
include .makefile/help.mk